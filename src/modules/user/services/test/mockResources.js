const expiry = '1d';
const secretKey = 'secret123';

const mockResources = {
    AuthService: {
        expiry: expiry,
        secretKey: secretKey,
        createToken: {
            POSITIVE_CASE_INPUT: {
                id: 1
            },
            POSITIVE_CASE_OUTPUT: {
                token: 'jwttoken'
            }
        }
    },
    PasswordManager: {
        hashPassword: {
            POSITIVE_CASE_INPUT: {
                password: 'password123'
            },
            POSITIVE_CASE_OUTPUT: {
                hashedPassword:
                    '$2a$12$7Y8z3oHZX38djsA4h9K94eRdW2qkMpdRC5i1rcCYFiUacJ9.fLVsS'
            }
        },
        compare: {
            POSITIVE_CASE_INPUT: {
                password: 'password123',
                hashedPassword:
                    '$2a$12$7Y8z3oHZX38djsA4h9K94eRdW2qkMpdRC5i1rcCYFiUacJ9.fLVsS'
            },
            POSITIVE_CASE_OUTPUT: true
        }
    },
    UserService: {
        register: {
            POSITIVE_CASE_INPUT: {
                email: 'email123@gmail.com',
                password: 'password123',
                role: 'admin',
                hashedPassword:
                    '$2a$12$7Y8z3oHZX38djsA4h9K94eRdW2qkMpdRC5i1rcCYFiUacJ9.fLVsS'
            },
            POSITIVE_CASE_OUTPUT: {
                id: 1,
                email: 'email123@gmail.com',
                password:
                    '$2a$12$7Y8z3oHZX38djsA4h9K94eRdW2qkMpdRC5i1rcCYFiUacJ9.fLVsS',
                role: 'admin'
            },
            POSITIVE_CASE_GET_USER_BY_EMAIL: null
        }
    }
};

module.exports = mockResources;
