const ReviewRepository = require('..//review.repository');
const Review = require('../../../../shared/database/models/review.model');
const mockResources = require('./mockResources');

// TODO: write ReviewRepository unit test
describe('ReviewRepository', () => {
    describe('ReviewRepository.__createReview', () => {
        it('should return review created', async () => {});
    });

    describe('ReviewRepository.__getMovieReviews', () => {
        it('should return list movie reviews', async () => {});
    });

    describe('ReviewRepository.__getReviewByUserIdAndMovieId', () => {
        it('should return review by userId and movieId', async () => {});
    });
});
