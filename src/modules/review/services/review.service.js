const ReviewRepository = require('../repositories/review.repository');
const UserService = require('../../user/services/user.service');
const MovieService = require('../../movie/services/movie.service');

class ReviewService {
    #reviewRepository;
    #userService;
    #movieService;

    constructor(reviewRepository, userService, movieService) {
        this.#reviewRepository = reviewRepository || new ReviewRepository();
        this.#userService = userService || new UserService();
        this.#movieService = movieService || new MovieService();
    }

    createReview = async ({ userId, movieId, rating, comment }) => {
        if (!userId || !movieId || !rating) {
            throw new Error('userId, movieId, and rating cannot be empty');
        }

        const user = await this.#userService.getUserById(userId);
        if (!user) {
            throw new Error('Invalid userId');
        }

        const movie = await this.#movieService.getMovieById(movieId);
        if (!movie) {
            throw new Error('Invalid movieId');
        }

        const review = await this.#reviewRepository.getReviewByUserIdAndMovieId(
            userId,
            movieId
        );
        if (review) {
            throw new Error('Review already exists');
        }

        return this.#reviewRepository.createReview({
            userId,
            movieId,
            rating,
            comment
        });
    };

    getMovieReviews = async ({ movieId, page = 1, perPage = 10 }) => {
        if (!movieId) {
            throw new Error('movieId cannot be empty');
        }

        if (page < 1 || perPage < 1) {
            throw new Error('Invalid page and/or perPage');
        }

        const movie = await this.#movieService.getMovieById(movieId);
        if (!movie) {
            throw new Error('Invalid movieId');
        }

        const reviews = await this.#reviewRepository.getMovieReviews({
            movieId,
            page,
            perPage
        });
        if (!reviews.length) {
            throw new Error('Review not found');
        }

        return reviews;
    };
}

module.exports = ReviewService;
