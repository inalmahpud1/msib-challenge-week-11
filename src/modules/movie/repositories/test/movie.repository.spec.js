const MovieRepository = require('../movie.repository');
const Movie = require('../../../../shared/database/models/movie.model');
const mockResources = require('./mockResources');

describe('MovieRepository', () => {
    describe('MovieRepository.__createMovie', () => {
        it('should return movie created', async () => {
            //arrange
            const mockInput =
                mockResources.MovieRepository.createMovie.POSITIVE_CASE_INPUT;
            const mockOutput =
                mockResources.MovieRepository.createMovie.POSITIVE_CASE_OUTPUT;

            Movie.create = jest.fn().mockResolvedValue(mockOutput);

            //act
            const result = await new MovieRepository().createMovie(mockInput);

            //assert
            expect(result).toEqual(mockOutput);
            expect(Movie.create).toHaveBeenCalledTimes(1);
            expect(Movie.create).toBeCalledWith(mockInput);
        });
    });

    describe('MovieRepository.__getMovies', () => {
        it('should return list movies', async () => {
            //arrange
            const mockInput =
                mockResources.MovieRepository.getMovies.POSITIVE_CASE_INPUT;
            const mockOutput =
                mockResources.MovieRepository.getMovies.POSITIVE_CASE_OUTPUT;
            const paramsFindAll =
                mockResources.MovieRepository.getMovies.PARAMS_FIND_ALL;

            Movie.findAll = jest.fn().mockResolvedValue(mockOutput);

            //act
            const result = await new MovieRepository().getMovies(mockInput);

            //assert
            expect(result).toEqual(mockOutput);
            expect(Movie.findAll).toHaveBeenCalledTimes(1);
            expect(Movie.findAll).toBeCalledWith(paramsFindAll);
        });
    });

    describe('MovieRepository.__getMovieById', () => {
        it('should return movie details', async () => {
            //arrange
            const mockInput =
                mockResources.MovieRepository.getMovieById.POSITIVE_CASE_INPUT;
            const mockOutput =
                mockResources.MovieRepository.getMovieById.POSITIVE_CASE_OUTPUT;

            Movie.findByPk = jest.fn().mockResolvedValue(mockOutput);

            //act
            const result = await new MovieRepository().getMovieById(
                mockInput.id
            );

            //assert
            expect(result).toEqual(mockOutput);
            expect(Movie.findByPk).toHaveBeenCalledTimes(1);
            expect(Movie.findByPk).toBeCalledWith(mockInput.id);
        });
    });
});
