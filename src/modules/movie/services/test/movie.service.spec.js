const MovieService = require('../movie.service');
const MovieRepository = require('../../repositories/movie.repository');
const mockResources = require('./mockResources');

describe('MovieService', () => {
    describe('MovieService.__createMovie', () => {
        it('should return movie created', async () => {
            //arrange
            const mockInput =
                mockResources.MovieService.createMovie.POSITIVE_CASE_INPUT;
            const mockOutput =
                mockResources.MovieService.createMovie.POSITIVE_CASE_OUTPUT;

            const movieRepository = new MovieRepository();
            movieRepository.createMovie = jest
                .fn()
                .mockResolvedValue(mockOutput);

            //act
            const result = await new MovieService(movieRepository).createMovie(
                mockInput
            );

            //assert
            expect(result).toEqual(mockOutput);
            expect(movieRepository.createMovie).toHaveBeenCalledTimes(1);
            expect(movieRepository.createMovie).toBeCalledWith(mockInput);
        });
    });

    describe('MovieService.__getMovies', () => {
        it('should return list movies', async () => {
            //arrange
            const mockInput =
                mockResources.MovieService.getMovies.POSITIVE_CASE_INPUT;
            const mockOutput =
                mockResources.MovieService.getMovies.POSITIVE_CASE_OUTPUT;

            const movieRepository = new MovieRepository();
            movieRepository.getMovies = jest.fn().mockResolvedValue(mockOutput);

            //act
            const result = await new MovieService(movieRepository).getMovies(
                mockInput
            );

            //assert
            expect(result).toEqual(mockOutput);
            expect(movieRepository.getMovies).toHaveBeenCalledTimes(1);
            expect(movieRepository.getMovies).toBeCalledWith(mockInput);
        });
    });

    describe('MovieService.__getMovieById', () => {
        it('should return movie details', async () => {
            //arrange
            const mockInput =
                mockResources.MovieService.getMovieById.POSITIVE_CASE_INPUT;
            const mockOutput =
                mockResources.MovieService.getMovieById.POSITIVE_CASE_OUTPUT;

            const movieRepository = new MovieRepository();
            movieRepository.getMovieById = jest
                .fn()
                .mockResolvedValue(mockOutput);

            //act
            const result = await new MovieService(movieRepository).getMovieById(
                mockInput.id
            );

            //assert
            expect(result).toEqual(mockOutput);
            expect(movieRepository.getMovieById).toHaveBeenCalledTimes(1);
            expect(movieRepository.getMovieById).toBeCalledWith(mockInput.id);
        });
    });
});
