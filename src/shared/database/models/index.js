const Movie = require('./movie.model');
const Review = require('./review.model');
const User = require('./user.model');

module.exports = {
    User,
    Movie,
    Review
};
