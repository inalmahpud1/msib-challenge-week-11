FROM node:18-alpine

# Create app directory
WORKDIR /movie-review-app

# Install app dependencies
COPY package*.json ./
RUN npm install

# Bundle app source
COPY . .

# Run application
CMD npm start